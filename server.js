//define vars
var firebase = require("firebase");
var Http = require( 'http' ),
    request = require('request'),
    restify = require('restify'),
    Router = require( 'router' ),
    url = require('url'),
    server,
    router;
var BodyParser = require( 'body-parser' );

//initialize firebase object
 var config = {
    apiKey: "AIzaSyBhlfiYK-odRmnTBM_FuzjfTQa0J9IRDPI",
    authDomain: "account-4c3f3.firebaseapp.com",
    databaseURL: "https://account-4c3f3.firebaseio.com",
    projectId: "account-4c3f3",
    storageBucket: "",
    messagingSenderId: "738616861946"
  };
  firebase.initializeApp(config);
  
//create server 
router = new Router();

server = Http.createServer( function( request, response ) {
  router( request, response, function( error ) {
          if ( !error ) {
             response.writeHead( 404 );
             } else {
             // Handle errors
                 console.log( error.message, error.stack );
               response.writeHead( 400 );
                 }
            response.end( '\n' );
           });
    });


router.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'https://cde305-part3.herokuapp.com');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

//server listen on port 8080
server.listen(process.env.PORT || 8080, function() {
  console.log( 'Listening on port 8080' );
});

router.use( BodyParser.json() );



//to post new Recipe item into firebase 
router.post( '/recipes', createItem );

function createItem( request, response ) {
    var data = request.body;
 // var params = url.parse(request.url, true).query;
    var recipeId = data.recipeId;
    var image = data.image;
    var uri = data.url;
    var title = data.title;
    var userId = data.userId;
    create(recipeId,image,uri,title,userId)
   
  
    response.writeHead( 201, {
         'Content-Type' : 'text/plain',
    });
    response.end(  );
}


//view recipe by recipeId
router.get( '/recipes/:recipeId', readItem );

function readItem( req, response ) {
     var id = req.params.recipeId;
    // var list={};
     var list ='{"recipes":[';
     var  dataurl="https://account-4c3f3.firebaseio.com/recipes/"+id+".json";
     request({
            url: dataurl,
            json: true
                }, function (error, res, body) {
                          if (!error && res.statusCode === 200) {
                              var obj;
                              if(body!=null){
                                obj = Object.keys(body).map(function(_) { return body[_]; });
                              }
                              console.log(body)
                              
                          //  list += '{"recipeId":"'+body.recipeId+'"}';
                          if(body!=null){
                           list += '{"recipeId":"'+body.recipeId+'","image":"'+body.image+'","url":"'+body.url+'","title":"'+body.title +'"}'
                          }
                          
                          list +="]}";
                          
                          if(body==null){
                              
                               list="null"
                              
                          }
                           console.log(list)
                      }else if(error){
                          list="null"
                      }
            })
   
     setTimeout(function() {
          response.write(list);
          response.end( );
                            }, 2000);
        }


//Delete recipe by its id
router.delete( '/recipes/:recipeId', deleteItem );
function deleteItem( request, response ) {
    var recipeId = request.params.recipeId;
    console.log( 'Delete recipe', recipeId);
    deleteRecipe(recipeId);
    response.writeHead( 200, {
            'Content-Type' : 'text/plain'
          });
    response.end( '' );
    }


//Return all recipe from firebase
router.get( '/recipes', readList );
function readList( req, response ) {
   // var list = {};
    var list ='{"recipes":[';
    request({
          url: "https://account-4c3f3.firebaseio.com/recipes.json",
          json: true
              }, function (error, res, body) {
                      if (!error && res.statusCode === 200) {
                           // list +=JSON.stringify(body);
                           var obj;
                           if(body!=null){
                            obj = Object.keys(body).map(function(_) { return body[_]; });
                            for (var i = 0; i < obj.length ;i++){
                                
                               list += '{"recipeId":"'+obj[i].recipeId+'","image":"'+obj[i].image+'","url":"'+obj[i].url+'","title":"'+obj[i].title +'","userId":"'+obj[i].userId+'"}';
                            if(i != obj.length-1){
                                list+=",";
                            }
                            }
                             list +="]}";
                           }else
                           {
                               list="null"
                           }
                            
                            
                           
                            console.log("this is list");
                            console.log(list)
                        }
            })
     response.writeHead( 200, {
          'Content-Type' : 'text/plain',
            });
 
     setTimeout(function() {
          response.write(list)
          response.end( );
                            }, 2000);
  }


// update recipes 
router.put( '/recipes/:recipeId', updateRecipe );
function updateRecipe( request, response ){
     var data = request.body;
     var recipeId = request.params.recipeId;
     var image = data.image;
     var url = data.url;
     var title = data.title;
     var userId = data.userId;
     update(recipeId,image,url,title,userId);
     console.log( 'Update recipes', recipeId );
     response.writeHead( 201, {
         'Content-Type' : 'text/plain',
             });
     response.end( recipeId );
}


// insert keywords to search recipes.
router.get( '/recipes/search/:keywords', searchRecipes2 );
// since the food2fork API is down the searchRecipes will change to searchRecipes2 by using another API
function searchRecipes(req,response){
    
      var keywords = req.params.keywords;
      
     var list ={};
     var string='{"recipes":[';
      request({
              url: "http://food2fork.com/api/search?key=10e968f1bc560367af178fd8b007240a&q="+keywords,
              json: true
         }, function (error, res, body) {
             if (!error && res.statusCode === 200) {
                    console.log("get item from API");
                   
                   var count=0;
                   console.log(body.count)
                   for(var i = 0 ; i < body.count;i++){
                       if(count < 5){
                           string+='{"recipeId":"'+body.recipes[i].recipe_id+'","title":"'+body.recipes[i].title+'","url":"'+body.recipes[i].f2f_url+'","image":"'+body.recipes[i].image_url+ '"}';
                           if(count!=4){
                               string+=',';
                           }
                       }
                       count++;
                   }
                    string +="]}";
                    list= string;
                   console.log(list);
                   
                  }
            })
       response.writeHead( 200, {
            'Content-Type' : 'text/plain',
             });
       setTimeout(function() {
          response.write(string)
          response.end( );
                            }, 2000);

}
//use new API to search for recipe since food2fork is down
function searchRecipes2(req,response){
    
      var keywords = req.params.keywords;
      
     var list ={};
     var string='{"recipes":[';
      request({
              url: "http://www.recipepuppy.com/api/?q="+keywords,
              json: true
         }, function (error, res, body) {
             if (!error && res.statusCode === 200) {
                    console.log("get item from API");
                   
                   var count=0;
                   console.log(body.count)
                   for(var i = 0 ; i < body.results.length;i++){
                       if(count < 5){
                           string+='{"recipeId":"'+Math.random().toString(36).substr(2, 5)+'","title":"'+body.results[i].title+'","url":"'+body.results[i].href+'","image":"'+body.results[i].thumbnail+ '"}';
                           if(count!=4){
                               string+=',';
                           }
                       }
                       count++;
                   }
                    string +="]}";
                    list= string;
                   console.log(list);
                   
                  }
            })
       response.writeHead( 200, {
            'Content-Type' : 'text/plain',
             });
       setTimeout(function() {
          response.write(string)
          response.end( );
                            }, 2000);

}

router.get( '/users', readuser );
function readuser(req,response){
    var list ='{"account":[';
    request({
          url: "https://account-4c3f3.firebaseio.com/account.json",
          json: true
              }, function (error, res, body) {
                      if (!error && res.statusCode === 200) {
                           // list +=JSON.stringify(body);
                           var obj;
                           if(body!=null){
                            obj = Object.keys(body).map(function(_) { return body[_]; });
                            for (var i = 0; i < obj.length ;i++){
                                
                               list += '{"email":"'+obj[i].email+'"}';
                            if(i != obj.length-1){
                                list+=",";
                            }
                            }
                             list +="]}";
                           }else
                           {
                               list="null"
                           }
                            
                        }
            })
     response.writeHead( 200, {
          'Content-Type' : 'text/plain',
            });
 
     setTimeout(function() {
          response.write(list)
          response.end( );
                            }, 2000);
    
}

//create a new user
router.post( '/user', createuser );
function createuser( request, response ) {
    var data = request.body;
 // var params = url.parse(request.url, true).query;
    var user = data.username;
    var email = data.email;
    var password = data.password;
    createUser(user,email,password)
    
    console.log( 'Create user', user  );
  
    response.writeHead( 201, {
         'Content-Type' : 'text/plain',
    });
    response.end(  );
}
router.get( '/user', login );
function login( req, response ) {

     var params = url.parse(req.url, true).query;
     var username = params.username;
     var password = params.password;
        
    // var list={};
     var list ='{"account":[';
     var  dataurl="https://account-4c3f3.firebaseio.com/account/"+username+".json";
     request({
            url: dataurl,
            json: true
                }, function (error, res, body) {
                          if (!error && res.statusCode === 200) {
                              var obj;
                              if(body!=null){
                                obj = Object.keys(body).map(function(_) { return body[_]; });
                              }
                              console.log(body)
                            if(body!=null){
                                if(body.password == password){
                                  list += '{"username":"'+body.user+'","email":"'+body.email+'","password":"'+body.password+'"}'
                                }
                            }
                          
                          list +="]}";
                          
                          if(body==null){
                              
                               list="null"
                              
                          }
                           console.log(list)
                      }else if(error){
                          list="null"
                      }
            })
   
     setTimeout(function() {
          response.write(list);
          response.end( );
                            }, 2000);
        }





//set a new value into firebase
function create(recipeId,image,url,title,userId){
	
    var db = firebase.database();
    var ref = db.ref("recipes/" + recipeId).set({
        recipeId:recipeId,
        image:image,
        url:url,
        title:title,
        userId:userId
        });
     console.log( 'Create recipe', recipeId," with id ", userId );
 }

//update a item from firebase by its id
function update(recipeId,image,url,title,userId){
    var db = firebase.database();
    var ref = db.ref("/recipes/");
    ref.child(recipeId).set({
        recipeId:recipeId,
        image:image,
        url:url,
        title:title,
        userId:userId
    });
}


//delete a recipe from firebase
function deleteRecipe(recipeId){
    var db = firebase.database();
    var ref = db.ref("/recipes/"+recipeId);
    ref.remove();
}



function createUser(user,email,password){
	
 var db = firebase.database();
      var ref = db.ref("account/" + user).set({
         user:user,
         email:email,
         password:password
        });
 
 }
 